export class Record {  
    id : string;
    name : string;
    age:number;
    sex:string;
    response1:boolean;
    response2:boolean;
    response3:boolean;
    submitDate:Date;
};
