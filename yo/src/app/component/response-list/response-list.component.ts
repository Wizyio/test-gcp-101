import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { RecordResources } from 'app/service/datastore-service/record.service';
import {Record} from 'app/model/Record';
import { fuseAnimations } from '@fuse/animations';
@Component({
  selector: 'app-response-list',
  templateUrl: './response-list.component.html',
  styleUrls: ['./response-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class ResponseListComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
    
  recordList: any;
  displayedColumns = ['name', 'age', 'sex', 'response1', 'response2', 'response3'];
  dataSource = new MatTableDataSource<Record>(this.recordList);
  
  constructor(
      public recordService:RecordResources
  ) {}

  ngOnInit(){
      this.getRecordList();
  }

  getRecordList(): void{
      this.recordService.listRecords().subscribe(res => {
          this.recordList = res
          this.dataSource = new MatTableDataSource<Record>(this.recordList);
          this.dataSource.paginator = this.paginator;
      });
  }

}
