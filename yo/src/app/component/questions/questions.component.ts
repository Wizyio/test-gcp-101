import { Component, OnInit, OnDestroy, AfterViewInit, ViewChildren, QueryList, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RecordResources } from 'app/service/datastore-service/record.service';
import {Record} from 'app/model/Record';
import { MatSnackBar } from '@angular/material';
import { BigqueryResources } from 'app/service/datastore-service/biquery.service';
import { Log } from 'app/model/Log';
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class QuestionsComponent implements OnInit, AfterViewInit {
  course: any = {
    totalSteps : 4
  }
  record:Record = new Record();
  log:Log = new Log();
  courseSubscription: Subscription;
  currentStep = 0;
  courseStepContent;
  animationDirection: 'left' | 'right' | 'none' = 'none';
  @ViewChildren(FusePerfectScrollbarDirective) fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

  constructor(
    private recordResources:RecordResources,
    private snackbar: MatSnackBar,
    private bigqueryResources:BigqueryResources
  ){}

  ngOnInit(){
    this.sendLog("home page", "user connect to home page", "");
  }

  sendLog(action, desc1, desc2){
    this.log.action = action;
    this.log.desc1 = desc1;
    this.log.desc2 = desc2;
    this.bigqueryResources.StreamBigQueryEmmLog(this.log).subscribe(res =>{

    })
  }

  sendQuestionnaire(record){
    if(record.name && record.age && record.sex && record.response1 != null && record.response2 != null && record.response3 != null){
      this.recordResources.saveRecord(record).subscribe(res=>{
        if(res == 'OK'){
          this.snackbar.open('Response saved', '', {
            verticalPosition: 'top',
            duration        : 2000,
            extraClasses    : ['success-toaster']
          });
          this.currentStep = 0;
          this.record = new Record();
        }else{
          this.snackbar.open('Error', '', {
            verticalPosition: 'top',
            duration        : 2000,
            extraClasses    : ['error-toaster']
          });
        }
      });
      this.sendLog("submit responses", "submit all the form", this.record.name);
    }else{
      this.snackbar.open('Some required information is missing or incomplete', '', {
        verticalPosition: 'top',
        duration        : 2000,
        extraClasses    : ['error-toaster']
      });
      this.sendLog("submit responses", "some required information is missing or incomplete", this.record.name);
    }
  }

  ngAfterViewInit(){
    this.courseStepContent = this.fuseScrollbarDirectives.find((fuseScrollbarDirective) => {
      return fuseScrollbarDirective.element.nativeElement.id === 'course-step-content';
    });
  }

  gotoStep(step){
    if((step < this.currentStep)||((this.currentStep == 0 && this.record.name && this.record.sex && this.record.age)||(this.currentStep == 1 && this.record.response1 != null)||(this.currentStep == 2 && this.record.response2 != null)||(this.currentStep == 3 && this.record.response3 != null))){
      // Decide the animation direction
      this.animationDirection = this.currentStep < step ? 'left' : 'right';

      // Run change detection so the change
      // in the animation direction registered
      // this.changeDetectorRef.detectChanges();

      // Set the current step
      this.currentStep = step;
      this.sendLog("go to step"+step, "", this.record.name);
    }else{
      this.snackbar.open('Some required information is missing or incomplete', '', {
        verticalPosition: 'top',
        duration        : 2000,
        extraClasses    : ['error-toaster']
      });
      this.sendLog("go to step"+step, "missing informations", this.record.name);
    }
  }

  gotoNextStep(){
    if((this.currentStep == 0 && this.record.name && this.record.sex && this.record.age)||(this.currentStep == 1 && this.record.response1 != null)||(this.currentStep == 2 && this.record.response2 != null)||(this.currentStep == 3 && this.record.response3 != null)){
      if ( this.currentStep === this.course.totalSteps - 1 ){
        return;
      }

      this.animationDirection = 'left';

      // Run change detection so the change
      // in the animation direction registered
      // this.changeDetectorRef.detectChanges();
      this.currentStep++;
      this.sendLog("next"+this.currentStep, "go to step"+this.currentStep, this.record.name);
    }else{
      this.sendLog("next"+this.currentStep, "missing informations to go to step"+this.currentStep, this.record.name);
    }
    
  }

  gotoPreviousStep(){
    if ( this.currentStep === 0 ){
        return;
    }

    // Set the animation direction
    this.animationDirection = 'right';

    // Run change detection so the change
    // in the animation direction registered
    // this.changeDetectorRef.detectChanges();

    // Decrease the current step
    this.currentStep--;
    this.sendLog("previous"+this.currentStep, "go to step"+this.currentStep, this.record.name);
  }
}