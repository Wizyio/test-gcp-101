import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatTableModule, MatPaginatorModule, MatDividerModule, MatFormFieldModule, MatSortModule, MatSnackBarModule, MatDatepicker, MatDatepickerModule, MatExpansionModule, MatRadioModule } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FuseSharedModule } from '@fuse/shared.module';
import { CdkTableModule } from '@angular/cdk/table';
import { FuseWidgetModule } from '@fuse/components';
import { NguiMapModule } from '@ngui/map';
import { AgmCoreModule } from '@agm/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';



import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { A11yModule } from '@angular/cdk/a11y';
import { BidiModule } from '@angular/cdk/bidi';
import { LayoutModule } from '@angular/cdk/layout';
import { ObserversModule } from '@angular/cdk/observers';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  
  imports: [
    CdkTableModule,
    MatSortModule,
    FuseSharedModule,
    MatTableModule,
    MatPaginatorModule,
    ScrollDispatchModule,
    A11yModule,
    BidiModule,
    LayoutModule,
    ObserversModule,
    OverlayModule,
    PortalModule,
    CdkStepperModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatChipsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatDividerModule,
    FuseWidgetModule,
    NguiMapModule,
    AgmCoreModule,
    NgxChartsModule,
    ChartsModule,
    MatDialogModule,
    MatSnackBarModule,
    AgmCoreModule,
    MatExpansionModule,
    MatDatepickerModule,
    NgxMatSelectSearchModule,
    MatRadioModule,
    NgxMatSelectSearchModule,
    MatRadioModule
  ],
  exports: [
    CdkTableModule,
    MatSortModule,
    FuseSharedModule,
    MatTableModule,
    MatPaginatorModule,
    ScrollDispatchModule,
    A11yModule,
    BidiModule,
    LayoutModule,
    ObserversModule,
    OverlayModule,
    PortalModule,
    CdkStepperModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatChipsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatDividerModule,
    FuseWidgetModule,
    NguiMapModule,
    AgmCoreModule,
    NgxChartsModule,
    ChartsModule,
    MatDialogModule,
    MatSnackBarModule,
    AgmCoreModule,
    MatExpansionModule,
    MatDatepickerModule,
    NgxMatSelectSearchModule,
    MatRadioModule
   ]
})
export class SharedModule {}
