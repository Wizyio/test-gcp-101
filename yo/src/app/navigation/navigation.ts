export const navigation = [
    {
        'id'      : 'application',
        'title'   : 'Application',
        'translate': 'NAV.APPLICATIONS',
        'type'    : 'group',
        'children': [  
            {
                'id'   : 'questions',
                'title': 'Questions',
                'translate': 'NAV.SAMPLE.TITLE',
                'type' : 'item',
                'icon' : 'question_answer',
                'url'  : '/questions',
            }    
        ]
    }
];
