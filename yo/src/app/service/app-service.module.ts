import { NgModule, ModuleWithProviders } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RecordResources } from 'app/service/datastore-service/record.service';
import { BigqueryResources } from 'app/service/datastore-service/biquery.service';

@NgModule({
    
})

export class AppServiceModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AppServiceModule,
            providers: [          
                RecordResources,
                BigqueryResources       
            ]
        }
    }
}