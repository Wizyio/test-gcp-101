import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RecordResources {

  constructor(private http: HttpClient) { }

  saveRecord(record){
    return this.http.post('/record/save' ,record).map(res =>  {
      return res as string;
    });  
  }
  getRecordByID(id){
    return this.http.get('/record/get/'+ id).map(res =>  {
      return res
    });
  }
  listRecords(){
    return this.http.get('/record/list').map(res =>  {
      return res
    });
  }
  countFilterRecordByResponse(resp){
    return this.http.get('/record/filter/'+ resp +'/count').map(res =>  {
      return res as any
    });
  }
  filterRecordBySex(){
    return this.http.get('/record/filter/sex').map(res =>  {
      return res as any
    });
  }
  filterRecordByAge(){
    return this.http.get('/record/filter/age').map(res =>  {
      return res as any
    });
  }
}
