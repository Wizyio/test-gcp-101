import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BigqueryResources {

  constructor(private http: HttpClient) { }

  StreamBigQueryEmmLog(log){
    return this.http.post('/bigquery/log' ,log).map(res =>  {
      return res as string;
    });  
  }
}
