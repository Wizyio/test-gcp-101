import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { fuseConfig } from './fuse-config';

import { AppComponent } from './app.component';
import { FuseMainModule } from './main/main.module';
import { FuseSampleModule } from './main/content/sample/sample.module';
import { AppServiceModule } from 'app/service/app-service.module';
import { AppRoutingModule } from 'app/app-routing.module';
import { NguiMapModule } from '@ngui/map';

import { SharedModule } from './shared.module';


@NgModule({
    declarations: [
        AppComponent,
      
    ],
    imports     : [   
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        
        TranslateModule.forRoot(),
        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseMainModule,
        FuseSampleModule,
        AppServiceModule.forRoot(),
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDJkh6iHUh5hvK7GQtEPUSq8I4NFVvZ0Bo'}),
        SharedModule,
    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule{}
