import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithComponentFactories } from '@angular/compiler/src/jit/compiler';
import { SharedModule } from 'app/shared.module';

///////////////////////////////////
////////////COMPONENTS/////////////
///////////////////////////////////
import { DashboardComponent } from 'app/component/dashboard/dashboard.component';
import { QuestionsComponent } from 'app/component/questions/questions.component';
import { ResponseListComponent } from 'app/component/response-list/response-list.component';


const routes: Routes = [
  {path: '', component: QuestionsComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'questions', component: QuestionsComponent},
];

@NgModule({
    declarations: [ 
        DashboardComponent,
        QuestionsComponent,
        ResponseListComponent
    ],
    entryComponents: [

    ],
    imports: [
        RouterModule.forRoot(routes, { useHash: true }),
        SharedModule
    ],
    exports: [
        RouterModule,
        DashboardComponent,
        QuestionsComponent,
        ResponseListComponent
    ]
})


export class AppRoutingModule {

}
